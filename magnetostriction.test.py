#!/usr/bin/env python

from magnetostriction import Magnetostriction
import dolfin
import numpy
import math
import itertools

import unittest



###############################################################################
# Define the elastic and magnetic parameters                                  #
###############################################################################

# Saturation magnetisation
# Dunlop + Ozdemir, p51
# (in A/m)
Ms = 480e3


# Cubic axes
e1 = (1.0, 0.0, 0.0)
e2 = (0.0, 1.0, 0.0)
e3 = (0.0, 0.0, 1.0)
e  = (e1, e2, e3)


# Elastic modulus
# http://link.springer.com/article/10.1007%2FBF03171417
# (in dynes/cm^2)
c11 = 27.5e11
c12 = 10.4e11
c44 = 9.65e11

# Convert to N/m^2
c11 = 0.1*c11
c12 = 0.1*c12
c44 = 0.1*c44

# Elasticity matrix, for use with voigt notation.
C = (
    (c11, c12, c12, 0.0, 0.0, 0.0),
    (c12, c11, c12, 0.0, 0.0, 0.0),
    (c12, c12, c11, 0.0, 0.0, 0.0),
    (0.0, 0.0, 0.0, c44, 0.0, 0.0),
    (0.0, 0.0, 0.0, 0.0, c44, 0.0),
    (0.0, 0.0, 0.0, 0.0, 0.0, c44)
)


# Magnetoelastic constants
# Dunlop + Ozdemir, p54
lmbda100 = 72.6e-6
lmbda111 = -19.5e-6

# Magnetoelastic coupling
# (in m^3/N)
B1 = lmbda100*(-3./2.)*(c11 - c12)
B2 = lmbda111*(-3.)*c44
B  = (B1,B2)



###############################################################################
# Define Magnetstriction class test suite                                     #
###############################################################################


class TestMagnetostriction(unittest.TestCase):

    def setUp(self, scale=1e-6, mesh_sampling=10, mesh_size=1):
        mesh = dolfin.UnitCubeMesh(mesh_sampling,mesh_sampling,mesh_sampling)
        mesh_coordinates = mesh.coordinates()
        mesh_coordinates *= mesh_size
        dx = dolfin.dx(domain = mesh.ufl_domain())

        self.scale = scale

        self.ms = Magnetostriction(Ms, e, B, C, mesh, dx, scale=scale)
        self.m = dolfin.Function(self.ms.V)

        self.tol = 1e-6


        # List the easy axes for magnetite
        self.easy_axes = (
            ( 1.0, 0.0, 0.0),
            ( 0.0, 1.0, 0.0),
            ( 0.0, 0.0, 1.0),

            (-1.0, 0.0, 0.0),
            ( 0.0,-1.0, 0.0),
            ( 0.0, 0.0,-1.0)
        )

        self.hard_axes = (
            ( 1.0/math.sqrt(3), 1.0/math.sqrt(3), 1.0/math.sqrt(3)),
            (-1.0/math.sqrt(3), 1.0/math.sqrt(3), 1.0/math.sqrt(3)),
            (-1.0/math.sqrt(3),-1.0/math.sqrt(3), 1.0/math.sqrt(3)),
            ( 1.0/math.sqrt(3),-1.0/math.sqrt(3), 1.0/math.sqrt(3)),

            ( 1.0/math.sqrt(3), 1.0/math.sqrt(3),-1.0/math.sqrt(3)),
            (-1.0/math.sqrt(3), 1.0/math.sqrt(3),-1.0/math.sqrt(3)),
            (-1.0/math.sqrt(3),-1.0/math.sqrt(3),-1.0/math.sqrt(3)),
            ( 1.0/math.sqrt(3),-1.0/math.sqrt(3),-1.0/math.sqrt(3))
        )

    ###########################################################################

    def analytic_minimised_strain(self, m):
        r'''
        The minimised strain, epsilon0, for a uniformly magnetised material.
        '''

        m = dolfin.as_vector(m)

        # Directional cosine, assume unit m,e
        alpha = lambda m, e: dolfin.dot(m,e)

        e1 = dolfin.Constant(e[0])
        e2 = dolfin.Constant(e[1])
        e3 = dolfin.Constant(e[2])

        a1 = alpha(m, e1)
        a2 = alpha(m, e2)
        a3 = alpha(m, e3)
        a = (a1,a2,a3)

        B1 = dolfin.Constant(B[0])
        B2 = dolfin.Constant(B[1])
        c11 = dolfin.Constant(C[0][0])
        c12 = dolfin.Constant(C[0][1])
        c44 = dolfin.Constant(C[3][3])

        def eps_diag(i,_):
            return (
                B1*(c12 - a[i]**2*(c11+2*c12))
                / ((c11-c12)*(c11+2*c12))
            )
        def eps_offd(i,j):
            return -0.5*B2*a[i]*a[j]/c44

        epsilon0 = lambda i,j: eps_diag(i,j) if i==j else eps_offd(i,j)
        epsilon0 = dolfin.as_matrix(
            [ [epsilon0(i,j) for j in range(3)] for i in range(3) ]
        )

        return epsilon0

    ###########################################################################

    def analytic_energy_density(self, m):
        r'''
        Analytic energy density for a uniformly magnetised material.
        '''

        epsilon0 = self.analytic_minimised_strain(m)
        #energy_density = self.ms.energy_density(m, epsilon0)

        B1 = self.ms.B[0]
        B2 = self.ms.B[1]

        c11 = self.ms.C[0,0]
        c12 = self.ms.C[0,1]
        c44 = self.ms.C[3,3]

        alpha = dolfin.as_vector((
            dolfin.dot(m, self.ms.e[0,:]),
            dolfin.dot(m, self.ms.e[1,:]),
            dolfin.dot(m, self.ms.e[2,:])
        ))

        energy_density = (
            B1*(
                  (alpha[0]**2)*epsilon0[0,0]
                + (alpha[1]**2)*epsilon0[1,1]
                + (alpha[2]**2)*epsilon0[2,2]
            )
            + B2*(
                  alpha[1]*alpha[2]*(epsilon0[1,2]+epsilon0[2,1])
                + alpha[0]*alpha[2]*(epsilon0[0,2]+epsilon0[2,0])
                + alpha[0]*alpha[1]*(epsilon0[0,1]+epsilon0[1,0])
            )
            + 0.5*c11*(
                epsilon0[0,0]**2 + epsilon0[1,1]**2 + epsilon0[2,2]**2
            )
            + 0.5*c44*(
                  (epsilon0[0,1]+epsilon0[1,0])**2
                + (epsilon0[1,2]+epsilon0[2,1])**2
                + (epsilon0[0,2]+epsilon0[2,0])**2
            )
            + c12*(
                  epsilon0[0,0]*epsilon0[1,1]
                + epsilon0[1,1]*epsilon0[2,2]
                + epsilon0[0,0]*epsilon0[2,2]
            )
        )

        return energy_density

    ###########################################################################

    def analytic_effective_field(self, m):
        m = dolfin.as_vector(m)

        # Assuming |m| = |e| = 1
        alpha = lambda m, e: dolfin.dot(m, e)
        # d/dM_i alpha(m, e)
        d_alpha = lambda m, e, i: e[i] - m[i]*alpha(m, e)

        K_lambda = dolfin.Constant(
            (9./4.)*( (c11-c12)*lmbda100**2 - 2.*c44*lmbda111**2 )
        )

        e0 = dolfin.Constant(e[0])
        e1 = dolfin.Constant(e[1])
        e2 = dolfin.Constant(e[2])

        def h_eff(i):
            return -1/dolfin.Constant(Ms)*K_lambda*(
                  2*d_alpha(m,e0,i)*alpha(m,e0) * alpha(m,e1)**2
                + alpha(m,e0)**2 * 2*d_alpha(m,e1,i)*alpha(m,e1) 

                + 2*d_alpha(m,e1,i)*alpha(m,e1) * alpha(m,e2)**2
                + alpha(m,e1)**2 * 2*d_alpha(m,e2,i)*alpha(m,e2)

                + 2*d_alpha(m,e2,i)*alpha(m,e2) * alpha(m,e0)**2
                + alpha(m,e2)**2 * 2*d_alpha(m,e0,i)*alpha(m,e0)
            )

        return dolfin.as_vector((h_eff(0), h_eff(1), h_eff(2)))

    ###########################################################################

    def test_uniform_solutions(self):
        r'''
        Compare solutions found with the Magnetostriction class
        versus analytic solutions for uniform magnetisations.
        '''

        #######################################################################
        # Compare solved and analytic solutions for uniform magnetisation.    #
        #######################################################################
        #
        # Check over easy/hard axes
        #
        for m in itertools.chain(self.easy_axes, self.hard_axes):

            self.m.interpolate(dolfin.Constant(m))
            self.ms.perform(self.m)

            E = self.ms.energy_density(self.m)
            Ea = self.analytic_energy_density(dolfin.Constant(m))

            S = dolfin.FunctionSpace(self.ms.mesh, "CG", 1)

            # Adding scaling factor to account for dx value that will be
            # used in dolfin.errornorm.
            error = dolfin.errornorm(
                u    = dolfin.project(dolfin.Constant(self.scale)*Ea, S),
                uh   = dolfin.project(dolfin.Constant(self.scale)*E, S),
                mesh = self.ms.mesh,
                degree_rise=0
            )

            energy_computed = dolfin.assemble(
                E*dolfin.Constant(self.scale)*self.ms.dx
            )
            energy_computed2 = self.ms.energy(self.m)
            energy_analytic = dolfin.assemble(
                Ea*dolfin.Constant(self.scale)*self.ms.dx
            )
            relative_error = error/energy_computed
            # print "Energy Computed 1:   % .16e"%(energy_computed)
            # print "Energy Computed 2:   % .16e"%(energy_computed2)
            # print "Energy Analytic:     % .16e"%(energy_analytic)
            # print "Error Norm:          % .16e"%(error)
            # print "Relative Error Norm: % .16e"%(relative_error)

            self.assertTrue(
                relative_error < self.tol, 
                "Computed energy not close to analytic energy."
                + " Relative Error: %s."%(relative_error)
            )


        #######################################################################
        # Compare solutions along equivalent axes.                            #
        #######################################################################
        # Check energies for all the easy axes match and all
        # the hard axes match.
        def get_uniform_energy(m):
            e = self.analytic_energy_density(dolfin.Constant(m))
            return dolfin.assemble(e*self.ms.dx)

        e_easy = get_uniform_energy(self.easy_axes[0])
        for m in self.easy_axes:
            e = get_uniform_energy(m)

            self.assertTrue(
                abs(e - e_easy) < self.tol, 
                "Easy axis energies not all the same!"
                + " Expected (%s), Got (%s)."%(e_easy, e)
            )

        e_hard = get_uniform_energy(self.hard_axes[0])
        for m in self.hard_axes:
            e = get_uniform_energy(m)

            self.assertTrue(
                abs(e - e_hard) < self.tol, 
                "Hard axis energies not all the same!"
                + " Expected (%s), Got (%s)."%(e_hard, e)
            )

    ###########################################################################

    def test_uniform_field(self):
        r'''
        For a uniform magnetisation, the magnetostrictive energy density
        can be expressed in the form
            e = K_lambda(alpha_1^2 alpha_2^2 + ...) + C,
        so the H field is
            H = d/dM e
              = K_lambda(2*(alpha_1)(d/dM(alpha_1)) alpha_2^2 + ...)

        We can compare computed solutions to this.
        '''

        theta_n = 5
        for theta_i in range(theta_n):
            theta = 2*math.pi*(float(theta_i)/theta_n)

            mx = math.cos(theta)
            my = math.sin(theta)

            self.m.interpolate(dolfin.Constant((mx,my,0.0)))
            self.ms.perform(self.m)
            h_eff = self.ms.h_eff
            #print "h_eff: ", h_eff

            h_eff_analytic = self.analytic_effective_field(self.m)
            h_eff_analytic_projection=dolfin.project(h_eff_analytic, self.ms.V)
            #print "h_eff_analytic: ", h_eff_analytic

            error = dolfin.errornorm(
                u = h_eff_analytic_projection,
                uh = h_eff,
                degree_rise = 0
            )

            self.assertTrue(
                error < self.tol,
                "Expected |H - h_analytic| close. Found difference of %s"%(
                    error
                )
                + " for uniform M = (%s,%s,%s)."%(
                    (mx, my, 0.0)
                )
            )

            #h_eff_norm = dolfin.norm(h_eff)
            #h_eff_analytic_norm = dolfin.norm(h_eff_analytic_projection)

            #print "Error Norm: ", error
            #print "h_eff norm: ", h_eff_norm
            #print "h_eff_analytic_norm: ", h_eff_analytic_norm

    ###########################################################################

    def test_derivatives_numerically(self):
        r'''
        Compare Heff computed with the Magnetostriction class against
        a discrete derivative computed using the energy.
        '''

        # Set up mesh sampling and scaling
        self.setUp(mesh_sampling=10, mesh_size=0.2, scale=1)

        # Define two magnetisations for evaluation and perturbation
        # of an energy
        m_center = dolfin.Function(self.ms.V)
        m_plus   = dolfin.Function(self.ms.V)
        m_minus  = dolfin.Function(self.ms.V)

        # Define a perturbation of the function m.
        # m_plus will be, for example, m_center + h*m_perturb.
        m_perturb = dolfin.Function(self.ms.V)

        # Define h_eff function for around the center
        e_grad_center = dolfin.Function(self.ms.V)


        # Define Energy Projection Space
        S = dolfin.FunctionSpace(self.ms.mesh, "CG", 1)

        # Define energy density functions
        ed_center = dolfin.Function(S)
        ed_plus   = dolfin.Function(S)
        ed_minus  = dolfin.Function(S)

        u = dolfin.TrialFunction(S)
        v = dolfin.TestFunction(S)

        # Classes for initialising and normalising M.
        class InitialM(dolfin.Expression):
            def eval_cell(self, vs, xs, cell):
                th = xs[0]*(math.pi/2.)
                vs[0] = math.cos(th)
                vs[1] = math.sin(th)
                vs[2] = 0.0

            def value_shape(self):
                return (3,)

        mesh_volume = dolfin.assemble(
            dolfin.Constant(1)*dolfin.dx(domain=self.ms.mesh.ufl_domain())
        )
        class PerturbM(dolfin.Expression):
            def eval_cell(self, vs, xs, cell):
                vs[0] = 1.0#/mesh_volume
                vs[1] = 0.0
                vs[2] = 0.0
            def value_shape(self):
                return (3,)

        class Normalize(dolfin.Expression):
            def __init__(self, M):
                self.M = M
            def eval_cell(self, vs, xs, cell):
                self.M.eval_cell(vs, xs, cell)
                vs /= math.sqrt(sum(v**2 for v in vs))
            def value_shape(self):
                return (3,)


        # Set m_center
        self.m.interpolate(InitialM())
        m_center.interpolate(Normalize(self.m))

        # Set m_perturb
        m_perturb.interpolate(PerturbM())

        # Find energy and energy gradient around m_center
        self.ms.perform(m_center)
        energy_center = self.ms.energy(m_center)
        e_grad_center.assign(-float(self.ms.Ms)*self.ms.h_eff)


        # Find differential of the energy functional using computed h_eff
        # Multiply by -Ms to turn heff into energy gradient
        h_int = dolfin.assemble(
            dolfin.inner(e_grad_center, m_perturb)*dolfin.dx
        )


        # Find numerical differentials of the energy functional for various h
        # Can't have h too small because it generates some numerical errors.
        # The range here is just trial and error, really.
        hs = [10**(-1-i) for i in range(6)]
        for h in hs:
            self.m.assign(m_center + 0.5*h*m_perturb)
            m_plus.interpolate(Normalize(self.m))
            self.m.assign(m_center - 0.5*h*m_perturb)
            m_minus.interpolate(Normalize(self.m))

            self.ms.perform(m_plus)
            energy_plus = self.ms.energy(m_plus)
            self.ms.perform(m_minus)
            energy_minus = self.ms.energy(m_minus)

            h_num_int_forwards  = (energy_plus   - energy_center)/(0.5*h)
            h_num_int_backwards = (energy_center - energy_minus )/(0.5*h)
            h_num_int_central   = (energy_plus   - energy_minus )/h


            # Check finite difference differential is within 1%
            # of the density derivative integral.
            h_int_diff = abs(1-h_num_int_central/h_int)

            # print "----------------------------"
            # print "E_center:            %20.15f"%(energy_center)
            # print "E_plus:              %20.15f"%(energy_plus)
            # print "E_minus:             %20.15f"%(energy_minus)
            # print "h:                   %20.15f"%(h)
            # print "h_ms_int             %20.15f"%(h_int)
            # print "h_num_int_forwards:  %20.15f"%(h_num_int_forwards)
            # print "h_num_int_backwards: %20.15f"%(h_num_int_backwards)
            # print "h_num_int_central:   %20.15f"%(h_num_int_central)
            # print "diff:                %20.15f"%(1 - h_num_int_central/h_int)
            # print "----------------------------"

            self.assertTrue(
                h_int_diff < 0.01,
                "Fractional difference between numerically computed"
                + " gradient and analytic gradient is too large!"
            )

    ###########################################################################

    def test_scaling(self):
        r'''
        Test that scaling the mesh coordinates and setting the scaling factor
        together makes no change to the outcome.
        '''

        # Use (1,1) scaling as the standard to test against.
        self.setUp(mesh_size=1, scale=1)

        import random
        random.seed(0)

        class MExpression(dolfin.Expression):
            r'''
            Define some random unit magnetisation.
            '''
            def eval_cell(self, vs, xs, cell):
                max_kick = 1.0
                vs[:] = [
                    random.uniform(-max_kick, max_kick),
                    random.uniform(-max_kick, max_kick),
                    random.uniform(-max_kick, max_kick)
                ]
                vs_len = math.sqrt(sum(v**2 for v in vs))
                if vs_len == 0:
                    vs[:] = [1.0,0.0,0.0]
                else:
                    vs[:] = [v/vs_len for v in vs]

            def value_shape(self):
                return (3,)

        # Assume that the shape of dofs for self.ms.V remains unchanged
        # between scalings.
        # Don't play with the mesh_sampling parameter of setUp and
        # this should be fine.
        S_unscaled = dolfin.FunctionSpace(self.ms.mesh, "CG", 1)
        V_unscaled = self.ms.V

        # m_expression to use for scaled and unscaled evaluation
        # This shouldn't be touched and only copied using .vector()
        # notation to copy only dofs and avoid non_matching_eval
        # and/or coordinate dependent evaluation.
        m_expression = dolfin.Function(V_unscaled)
        m_expression.interpolate(MExpression())

        # Unscaled values to use to compare to scaled ones.
        energy_unscaled         = 0.0
        # energy_density_unscaled = dolfin.Function(S_unscaled)
        h_eff_unscaled          = dolfin.Function(self.ms.V)

        # Calculate unscaled Magnetostriction
        self.m.vector().zero()
        self.m.vector().axpy(1.0, m_expression.vector())
        self.ms.perform(self.m)

        # Get unscaled values
        energy_unscaled = self.ms.energy(self.m)

        energy_density_unscaled_expr = self.ms.energy_density(self.m)
        energy_density_unscaled = dolfin.project(
            energy_density_unscaled_expr, S_unscaled
        )

        h_eff_unscaled.interpolate(self.ms.h_eff)


        mesh_unscaled = self.ms.mesh
        for scale in [10**(i) for i in range(-3,3)]:

            # Resetup with scaled mesh and scale set in Magnetostriction
            #self.setUp(mesh_size=1.0/scale, scale=scale)
            mesh_scaled = dolfin.Mesh(mesh_unscaled)
            mesh_scaled_coordinates = mesh_scaled.coordinates()
            mesh_scaled_coordinates *= (1.0/scale)

            self.ms = Magnetostriction(
                Ms, e, B, C,
                mesh_scaled, dolfin.dx(domain = mesh_scaled.ufl_domain()),
                scale=scale
            )
            self.m = dolfin.Function(self.ms.V)

            # Probably need new S and V to project values for evaluating
            # expressions.
            S_scaled = dolfin.FunctionSpace(self.ms.mesh, "CG", 1)
            V_scaled = self.ms.V

            # Calculate scaled Magnetostriction
            # Using .vector() expression to directly copy dofs
            self.m.vector().zero()
            self.m.vector().axpy(1.0, m_expression.vector())
            self.ms.perform(self.m)

            # Get scaled values
            energy_scaled = self.ms.energy(self.m)

            energy_density_scaled_expr = self.ms.energy_density(self.m)
            energy_density_scaled = dolfin.project(
                energy_density_scaled_expr, S_scaled
            )

            h_eff_scaled = dolfin.interpolate(self.ms.h_eff, V_scaled)


            # Find difference in energy values
            # Do |v1 - v2|/|v1|
            energy_diff = (
                math.sqrt((energy_unscaled - energy_scaled)**2)
                / abs(energy_unscaled)
            )
            print "Energy unscaled: % .16e"%(energy_unscaled)
            print "Energy scaled:   % .16e"%(energy_scaled)

            self.assertTrue(
                energy_diff < self.tol,
                "Total energy difference between scaled and unscaled"
                + " mesh is too big!"
            )


            # Find difference in energy density values
            # Do |v1 - v2|/|v1|
            energy_density_diff = dolfin.Vector(
                energy_density_scaled.vector()
            )
            energy_density_diff.axpy(
                -1.0, energy_density_unscaled.vector()
            )
            energy_density_diff_norm = (
                energy_density_diff.norm("l2")
                / energy_density_unscaled.vector().norm("l2")
            )

            self.assertTrue(
                energy_density_diff_norm < self.tol,
                "Difference in energy densities between scaled and unscaled"
                + " mesh is too big!"
            )


            # Find difference in h_eff values
            # Do |v1 - v2|/|v1|
            h_eff_diff = dolfin.Vector(h_eff_scaled.vector())
            h_eff_diff.axpy(-1.0, h_eff_unscaled.vector())
            h_eff_diff_norm = \
                h_eff_diff.norm("l2") / h_eff_unscaled.vector().norm("l2")

            self.assertTrue(
                h_eff_diff_norm < self.tol,
                "Difference in Heff between scaled and unscaled mesh"
                + " is too big!"
            )

    ###########################################################################

    @unittest.skip("TODO: Fix finding mechanical rhs/lhs")
    def test_mechanical_equilibrium(self):
        r'''
        Test that after calling perform, the mechanical equilibrium
        equations hold for the computed u0.
        '''

        # TODO:
        # This could probably do with either several different MExpressions
        # or different scales...

        import random
        random.seed(0)
        class MExpression(dolfin.Expression):
            def eval_cell(self, vs, xs, cell):
                vs[:] = [
                    random.uniform(-1, 1),
                    random.uniform(-1, 1),
                    random.uniform(-1, 1)
                ]
                len_vs = math.sqrt(sum(v**2 for v in vs))
                vs[:] = [v/len_vs for v in vs]

            def value_shape(self):
                return (3,)

        m_expression = dolfin.Function(self.ms.V)
        m_expression.interpolate(MExpression())

        self.m.interpolate(m_expression)
        self.ms.perform(self.m)

        u0 = dolfin.Function(self.ms.u0)

        scale = self.ms.scale
        C = self.ms.C
        e = self.ms.e
        B = self.ms.B

        #mechanical_lhs = self.ms.elasticity_lhs(
        #    u0, u0, self.ms.C, scale = scale
        #)
        import ufl
        mechanical_lhs = self.ms.A_elasticity*u0.vector()
        print mechanical_lhs
        print dir(mechanical_lhs)

        mechanical_rhs = self.ms.l_magnetostriction
        print mechanical_rhs
        #mechanical_rhs = self.ms.magnetostriction_rhs(
        #    self.m, e, B, C, u0, scale = scale
        #)

        #lhs = dolfin.assemble(mechanical_lhs*self.ms.dx)
        #rhs = dolfin.assemble(mechanical_rhs*self.ms.dx)
        lhs = mechanical_lhs
        rhs = mechanical_rhs

        print "LHS: ", lhs.norm("l2")
        print "RHS: ", rhs.norm("l2")
        norm = (lhs - rhs).norm("l2")/rhs.norm("l2")
        print "NORM: ", norm

        self.assertTrue(
            norm < self.tol,
            "LHS != RHS in mechanical equilibrium equations!"
        )

    ###########################################################################

    def test_displacement_minimisation(self):
        r'''
        Also check that the computed u0 provides the lowest possible energy
        for the given M.
        '''

        import random
        random.seed(0)

        class RandomVectorExpression(dolfin.Expression):
            def __init__(self, vector_len = 1.0):
                self._vector_len = vector_len

            def eval_cell(self, vs, xs, cell):
                vs[:] = [
                    random.uniform(-1, 1),
                    random.uniform(-1, 1),
                    random.uniform(-1, 1)
                ]
                len_vs = math.sqrt(sum(v**2 for v in vs))
                vs[:] = [self._vector_len*(v/len_vs) for v in vs]

            def value_shape(self):
                return (3,)


        # Run the magnetostriction solver to generate u0
        self.m.interpolate(RandomVectorExpression())
        self.ms.perform(self.m)


        # Generate u0+h, u0-h
        h = dolfin.Function(self.ms.V)
        u0_p = dolfin.Function(self.ms.V)
        u0_m = dolfin.Function(self.ms.V)

        h.interpolate(RandomVectorExpression(vector_len = 1e-16))
        u0_p.assign(self.ms.u0 + h)
        u0_m.assign(self.ms.u0 - h)


        # Get energy for u0.
        energy_0 = self.ms.energy(self.m)

        # Get energies for u0+h, u0-h
        energy_p = self.ms.energy(self.m, u0_p)
        energy_m = self.ms.energy(self.m, u0_m)

        # Check energies for u != u0 are larger than energy for u0.
        self.assertTrue(
            energy_p > energy_0,
            "Energy for (u0 + h) is larger than energy for u0!\n"
            + "E0: %.15e"%(energy_0) + "E+: %.15e"%(energy_p)
        )

        self.assertTrue(
            energy_m > energy_0,
            "Energy for (u0 - h) is larger than energy for u0!\n"
            + "E0: %.15e"%(energy_0) + "E-: %.15e"%(energy_m)
        )


if __name__ == '__main__':
    unittest.main()
