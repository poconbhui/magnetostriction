"""
This demo program solves a simple static magnetostrictive problem
(but is probably wrong).
"""

import dolfin
import ufl
import numpy as np


from ufl.algorithms import load_ufl_file
import os
directory = os.path.dirname(os.path.abspath(__file__))
default_magnetostriction_ufl_filename = \
    os.path.join(directory, "magnetostriction.ufl")
#magnetostriction_ufl = load_ufl_file(
#    os.path.join(directory, "magnetostriction.ufl")
#)
#print magnetostriction_ufl.object_names


class Magnetostriction:
    r'''
    The `Magnetostriction` class models the magnetostrictive effect in
    cubic crystals. It solves the minimum energy problem for a
    fixed magnetisation in a coupled magnetic and elastic system.
    It also generates an effective H field by finding derivatives of
    this minimum with respect to the magnetisation.
    '''

    # Mesh units in microns by default
    DEFAULT_SCALE = 1e-6

    # Default filename for magnetostriction.ufl
    DEFAULT_MAGNETOSTRICTION_FILENAME = default_magnetostriction_ufl_filename


    ###########################################################################


    def __init__(
        self,
        Ms, e, B, C,
        mesh, dx, V = None,
        scale = None,
        magnetostriction_ufl_filename = None
    ):
        r'''
        Assign and allocate all the parameters and Dolfin vectors necessary
        to solve the magnetostriction problem when given a magnetisation.
        This should avoid any memory allocations later.

        Input:
            Ms    - The saturation magnetisation, a scalar value.
            e     - The axes of easy magnetisation in the form
                e = (
                    (e1x, e1y, e1z),
                    (e2x, e2y, e2z),
                    (e3x, e3y, e3z)
                )
                ie something dolfin.Constant will recognize as a (3,3) shape,
                or as a dolfin/ufc compatible matrix indexable as e[i,:].
            B     - The magnetostrictive coupling constants in the form
                B = (B1, B2)
                or as a dolfin/ufc compatible vector indexable as B[i]
            C     - The elastic stiffness matrix in voigt notation. Typically
                for a cubic crystal
                C = (
                    (c11, c12, c12, 0.0, 0.0, 0.0),
                    (c12, c11, c12, 0.0, 0.0, 0.0),
                    (c12, c12, c11, 0.0, 0.0, 0.0),
                    (0.0, 0.0, 0.0, c44, 0.0, 0.0),
                    (0.0, 0.0, 0.0, 0.0, c44, 0.0),
                    (0.0, 0.0, 0.0, 0.0, 0.0, c44)
                )
                ie something dolfin.Constant will recognize as a (6,6) shape,
                or as a dolfin/ufc compatible matrix indexable as C[i,j].
            mesh  - The mesh to use for solving the magnetostrictive problem.
            dx    - The measure to use when solving the magnetostrictive
                problem.
            V     - The function space to use for the displacement and Heff.
                By default, it generates a CG 1 vector function space from
                the input mesh.
            scale - The units the mesh coordinates are in. By default, this
                is microns (1e-6). If the coordinates are in, say, nanometers,
                specify scale = 1e-9. For meters, scale = 1.
        '''


        #######################################################################
        # Initialize the arguments                                            #
        #######################################################################
        #
        # Set input values do dolfin.Constant's unless they already seem
        # to be dolfin/ufl compatible objects.
        # We want to do this to avoid recompilation if input constants are
        # changed, but still allow for function types (eg non-uniform Ms)
        # to be passed in.
        #

        def make_dolfin_friendly(x):
            if not isinstance(x, (ufl.expr.Expr, dolfin.cpp.GenericFunction)):
                return dolfin.Constant(x)
            else:
                return x

        self.Ms   = make_dolfin_friendly(Ms)
        self.e    = make_dolfin_friendly(e)
        self.B    = make_dolfin_friendly(B)
        self.C    = make_dolfin_friendly(C)
        self.mesh = mesh
        self.dx   = dx
        self.V    = V
        self.scale = make_dolfin_friendly(
            scale or Magnetostriction.DEFAULT_SCALE
        )
        self.dx_tot = dolfin.dx

        self.magnetostriction_ufl_filename = \
            magnetostriction_ufl_filename \
            or Magnetostriction.DEFAULT_MAGNETOSTRICTION_FILENAME

        self.magnetostriction_ufl = load_ufl_file(
            self.magnetostriction_ufl_filename
        )


        #######################################################################
        # Setup the spaces and functions                                      #
        #######################################################################
        #
        # Define the trial and test functions along with the solution
        # functions for the mechanical and magnetic solvers.
        #

        if not isinstance(V, dolfin.cpp.FunctionSpace):
            self.V = dolfin.VectorFunctionSpace(mesh, "CG", 1)

        # The test and trial functions
        self.u = dolfin.TrialFunction(self.V)
        self.v = dolfin.TestFunction(self.V)

        # The solution functions
        self.u0    = dolfin.Function(self.V)
        self.h_eff = dolfin.Function(self.V)


        #######################################################################
        # Set up the UFL forms with the input values.                         #
        #######################################################################
        #
        # Specialize the forms with the input values.
        # Where every variable can't be set (ie m), set as many as possible.
        #

        self.mechanical_a_form = ufl.replace(
            self.magnetostriction_ufl.object_by_name["mechanical_a"],
            {
                self.magnetostriction_ufl.object_by_name["u"]: self.u,
                self.magnetostriction_ufl.object_by_name["v"]: self.v,
                self.magnetostriction_ufl.object_by_name["c"]: self.C,
                self.magnetostriction_ufl.object_by_name["scale"]: self.scale,
            }
        )

        self.mechanical_L_form_partial = ufl.replace(
            self.magnetostriction_ufl.object_by_name["mechanical_L"],
            {
                self.magnetostriction_ufl.object_by_name["v"]:  self.v,
                self.magnetostriction_ufl.object_by_name["b"]:  self.B,
                self.magnetostriction_ufl.object_by_name["e1"]: self.e[0,:],
                self.magnetostriction_ufl.object_by_name["e2"]: self.e[1,:],
                self.magnetostriction_ufl.object_by_name["e3"]: self.e[2,:],
                self.magnetostriction_ufl.object_by_name["scale"]: self.scale,
            }
        )


        self.magnetic_a_form = ufl.replace(
            self.magnetostriction_ufl.object_by_name["magnetic_a"],
            {
                self.magnetostriction_ufl.object_by_name["u"]: self.u,
                self.magnetostriction_ufl.object_by_name["v"]: self.v,
                self.magnetostriction_ufl.object_by_name["scale"]: self.scale,
            }
        )

        self.magnetic_L_form_partial = ufl.replace(
            self.magnetostriction_ufl.object_by_name["magnetic_L"],
            {
                self.magnetostriction_ufl.object_by_name["Ms"]: self.Ms,
                self.magnetostriction_ufl.object_by_name["u0"]: self.u0,
                self.magnetostriction_ufl.object_by_name["v"]:  self.v,
                self.magnetostriction_ufl.object_by_name["b"]:  self.B,
                self.magnetostriction_ufl.object_by_name["c"]:  self.C,
                self.magnetostriction_ufl.object_by_name["e1"]: self.e[0,:],
                self.magnetostriction_ufl.object_by_name["e2"]: self.e[1,:],
                self.magnetostriction_ufl.object_by_name["e3"]: self.e[2,:],
                self.magnetostriction_ufl.object_by_name["scale"]: self.scale,
            }
        )

        self.energy_density_form = ufl.replace(
            self.magnetostriction_ufl.object_by_name["energy_density"],
            {
                self.magnetostriction_ufl.object_by_name["Ms"]: self.Ms,
                self.magnetostriction_ufl.object_by_name["b"]:  self.B,
                self.magnetostriction_ufl.object_by_name["c"]:  self.C,
                self.magnetostriction_ufl.object_by_name["e1"]: self.e[0,:],
                self.magnetostriction_ufl.object_by_name["e2"]: self.e[1,:],
                self.magnetostriction_ufl.object_by_name["e3"]: self.e[2,:],
                self.magnetostriction_ufl.object_by_name["scale"]: self.scale
            }
        )

        self.energy_form = ufl.replace(
            self.magnetostriction_ufl.object_by_name["energy"],
            {
                self.magnetostriction_ufl.object_by_name["Ms"]: self.Ms,
                self.magnetostriction_ufl.object_by_name["b"]:  self.B,
                self.magnetostriction_ufl.object_by_name["c"]:  self.C,
                self.magnetostriction_ufl.object_by_name["e1"]: self.e[0,:],
                self.magnetostriction_ufl.object_by_name["e2"]: self.e[1,:],
                self.magnetostriction_ufl.object_by_name["e3"]: self.e[2,:],
                self.magnetostriction_ufl.object_by_name["scale"]: self.scale
            }
        )


        #######################################################################
        # Assemble/Allocate matrices and vectors for the solvers              #
        #######################################################################
        #
        # Avoid having to allocate matrices and vectors for each solve.
        #

        # Allocate the linear form tensors (RHS)
        self.mechanical_L_vector = dolfin.Vector(self.u0.vector())
        self.magnetic_L_vector   = dolfin.Vector(self.h_eff.vector())

        # Assemble the bilinear forms
        self.mechanical_a_matrix = dolfin.assemble(self.mechanical_a_form)
        self.magnetic_a_matrix   = dolfin.assemble(self.magnetic_a_form)


        #######################################################################
        # Set up the Krylov solvers                                           #
        #######################################################################
        #
        # Solvers will solve matrix equations Ax = b.
        # The matrix A will be the LHS of the differential eqn,
        # the vector b will be the RHS and x the solution of the eqn.
        #

        # Set the solver for the assembled mechanical_a
        self.mechanical_solver = dolfin.KrylovSolver(
            self.mechanical_a_matrix, "cg"
        )

        # Generate normalized vectors for the null space of mechanical_a.
        mechanical_null_space_expressions = \
            self.magnetostriction_ufl.object_by_name["mechanical_null_space"]

        # Split expressions into list
        mechanical_a_matrix_null_space_expressions = [
            mechanical_null_space_expressions[i,:]
            for i in range(mechanical_null_space_expressions.shape()[0])
        ]

        # A list of vectors that will define the null space basis
        mechanical_a_matrix_null_space_vectors = [
            dolfin.Vector(self.u0.vector())
            for i in mechanical_a_matrix_null_space_expressions
        ]

        # UFL's cell.x doesn't support the dP measure, so we can't
        # just project the values directly onto a vector.
        # We need to use an actual projection solve instead.
        projection_a_form = ufl.dot(self.u, self.v)*ufl.dx
        projection_a = dolfin.assemble(projection_a_form)
        projection_L = dolfin.Vector(self.u0.vector())

        for vector, expression in zip(
            mechanical_a_matrix_null_space_vectors,
            mechanical_a_matrix_null_space_expressions
        ):
            projection_L_form = ufl.dot(expression, self.v)*ufl.dx
            dolfin.assemble(projection_L_form, tensor=projection_L)

            dolfin.solve(projection_a, vector, projection_L, "cg")
            vector *= 1.0/vector.norm("l2")


        # Set the null space for the mechanical solver
        self.mechanical_a_matrix_null_space = dolfin.VectorSpaceBasis(
            mechanical_a_matrix_null_space_vectors
        )
        self.mechanical_solver.set_nullspace(
            self.mechanical_a_matrix_null_space
        )


        # Set the solver for the assembled mechanical_a
        self.magnetic_solver = dolfin.KrylovSolver(
            self.magnetic_a_matrix, "cg"
        )


    ###########################################################################


    def energy_density(self, m, u0 = None):
        r'''
        Compute the energy density function for a given magnetisation, m,
        and strain, epsilon, using the magnetoelastic parameters given
        during initialisation.

        If no epsilon is given, use the last computed value.
        '''

        u0 = u0 or self.u0

        energy_density_form_full = ufl.replace(
            self.energy_density_form,
            {
                self.magnetostriction_ufl.object_by_name["m"]:  m,
                self.magnetostriction_ufl.object_by_name["u0"]: u0
            }
        )

        return energy_density_form_full


    ###########################################################################


    def energy(self, m, u0 = None):
        r'''
        Compute the total energy of a magnetoelastic system due to a
        magnetisation, m, and a strain, epsilon.

        If no strain is given, use the last computed value.
        '''

        u0 = u0 or self.u0

        # Find the magnetostrictive energy
        energy_form_full = ufl.replace(
            self.energy_form,
            {
                self.magnetostriction_ufl.object_by_name["m"]:  m,
                self.magnetostriction_ufl.object_by_name["u0"]: u0
            }
        )
        E = dolfin.assemble(energy_form_full)


        # print "Found Energy: ", E
        # print "Found u0: ", self.u0.vector().array()
        # print "Found A_elasticity: ", self.A_elasticity.array()

        return E


    ###########################################################################


    def perform(self, m):
        r'''
        Find the effective field H due to the magnetoelastic effect for
        a given magnetisation m in a cubic crystal.
        '''

        #######################################################################
        # Solve the mechanical equilibrium problem                            #
        #######################################################################

        # Set up the RHS
        mechanical_L_form_full = ufl.replace(
            self.mechanical_L_form_partial,
            { self.magnetostriction_ufl.object_by_name["m"]: m }
        )

        dolfin.assemble(
            mechanical_L_form_full, tensor = self.mechanical_L_vector
        )


        # Run the solver
        self.mechanical_a_matrix_null_space.orthogonalize(
            self.mechanical_L_vector
        )
        self.mechanical_solver.solve(
            self.u0.vector(), self.mechanical_L_vector
        )


        #######################################################################
        # Find H_eff                                                          #
        #######################################################################

        # Set up the RHS
        magnetic_L_form_full = ufl.replace(
            self.magnetic_L_form_partial,
            { self.magnetostriction_ufl.object_by_name["m"]: m }
        )
        dolfin.assemble(magnetic_L_form_full, tensor = self.magnetic_L_vector)

        # Run the solver
        self.magnetic_solver.solve(
            self.h_eff.vector(), self.magnetic_L_vector
        )

        return self.h_eff



###############################################################################
# Run example magnetisation and H_eff                                         #
###############################################################################

if __name__ == '__main__':

    import math

    ###########################################################################
    # Define the elastic and magnetic parameters                              #
    ###########################################################################

    # Saturation magnetisation
    # Dunlop + Ozdemir, p51
    # (in A/m)
    Ms = 480e3


    # Cubic axes
    e1 = (1.0, 0.0, 0.0)
    e2 = (0.0, 1.0, 0.0)
    e3 = (0.0, 0.0, 1.0)
    e = dolfin.as_matrix((e1, e2, e3))


    # Elastic modulus
    # http://link.springer.com/article/10.1007%2FBF03171417
    # (in dynes/cm^2)
    c11 = 27.5e11
    c12 = 10.4e11
    c44 = 9.65e11

    # Convert to N/m^2
    c11 = 0.1*c11
    c12 = 0.1*c12
    c44 = 0.1*c44

    # Elasticity matrix, for use with voigt notation.
    C = dolfin.as_matrix((
        (c11, c12, c12, 0.0, 0.0, 0.0),
        (c12, c11, c12, 0.0, 0.0, 0.0),
        (c12, c12, c11, 0.0, 0.0, 0.0),
        (0.0, 0.0, 0.0, c44, 0.0, 0.0),
        (0.0, 0.0, 0.0, 0.0, c44, 0.0),
        (0.0, 0.0, 0.0, 0.0, 0.0, c44)
    ))


    # Magnetoelastic constants
    # Dunlop + Ozdemir, p54
    lmbda100 = 72.6e-6
    lmbda111 = -19.5e-6

    # Magnetoelastic coupling
    # (in m^3/N)
    B1 = lmbda100*(-3./2.)*(c11 - c12)
    B2 = lmbda111*(-3.)*c44
    B = dolfin.as_vector((B1,B2))


    mesh = dolfin.UnitCubeMesh(10,10,10)
    dx = dolfin.dx(mesh)

    ms = Magnetostriction(Ms, e, B, C, mesh, dx)

    V = dolfin.VectorFunctionSpace(mesh, "CG", 1, dim = 3)
    m = dolfin.Function(V)

    class MVortex(dolfin.Expression):
        def eval(self, vs, xs):
            phi_max = math.pi/2.

            phi = xs[2]*phi_max
            vs[0] = math.cos(phi)*1.0 - math.sin(phi)*0.0
            vs[1] = math.sin(phi)*1.0 + math.cos(phi)*0.0
            vs[2] = 0.0

        def value_shape(self):
            return (3,)

    #m.interpolate(MVortex())
    vect = [1.0, 0.0, 0.0]
    vect[1] += 0.2
    vect = [v*math.sqrt(sum(vi for vi in vect)) for v in vect]
    #m.interpolate(dolfin.Constant(vect))
    m.interpolate(MVortex())
    h_eff = ms.perform(m)
    dolfin.plot(m)
    dolfin.plot(h_eff)
    dolfin.plot(ms.u0, mode="displacement")
    dolfin.interactive()
